var connection = new WebSocket('ws://'+location.hostname+':81/', ['arduino']);
connection.onopen = function () {
    connection.send('Connect ' + new Date());
};
connection.onerror = function (error) {
    console.log('WebSocket Error ', error);
};
connection.onmessage = function (e) {  
    console.log('From Server: ', e.data);
    document.getElementById('batt').value = e.data;
};
connection.onclose = function(){
    console.log('WebSocket connection closed');
};

function sendSpeed() {
    var l = document.getElementById('L').value;
    var r = document.getElementById('R').value;
    
    var speed = l << 16 | r;
    var speedstr = '#'+ speed.toString(16);
    console.log('SPEED: ' + speedstr); 
    connection.send(speedstr);
}

function stopTank(){
    document.getElementById('L').value = 1500;
    document.getElementById('R').value = 1500;
    sendSpeed();
}
