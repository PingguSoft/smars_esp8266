#ifndef _SERVERS_H_
#define _SERVERS_H_
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ArduinoOTA.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <FS.h>
#include <WebSocketsServer.h>
#include "common.h"
#include "utils.h"


#define STR_SOFTAP_SSID     "ESP8266_Tank"                      // The name of the Wi-Fi network that will be created
#define STR_SOFTAP_PASSWD   NULL                                // The password required to connect to it, NULL for an open network

#define STR_OTA_NAME        "ESP8266_Tank"                      // A name and a password for the OTA service
#define STR_OTA_PASSWD      "tank"

#define STR_mDNS_NAME       "esp8266"                           // Domain name for the mDNS responder

#define STR_AP1_SSID        "powerteam"
#define STR_AP1_PASSWD      "powerteam@!"

#define STR_AP2_SSID        "Hydra"
#define STR_AP2_PASSWD      "K5x48Vz3"

#define STR_AP3_SSID        "TJ's House"
#define STR_AP3_PASSWD      "cafebabe12"


class Servers
{
public:
    Servers()
    {
    }
    void setup(void);
    void loop(void);

private:
    ESP8266WiFiMulti mWiFiMulti;                             // Create an instance of the ESP8266WiFiMulti class, called 'mWiFiMulti'
    ESP8266WebServer mWebServer;                             // create a web mWebServer on port 80
    File             mFileUpload;                            // a File variable to temporarily store the received file

    void startWiFi(void);
    void startOTA(void);
    void startSPIFFS(void);
    void startWebSocket(void);
    void startMDNS(void);
    void handleNotFound(void);
    void handleFileList(void);
    bool handleFileRead(String path);
    void handleFileUpload();
    void handleFileDelete();
    void handleFileCreate();
    String formatBytes(size_t bytes);
    String getContentType(String filename);
};
#endif