/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/
#include <Arduino.h>
#include "ControlUDP.h"
#include "utils.h"

ControlUDP::ControlUDP()
{

}

ControlUDP::~ControlUDP()
{

}

void ControlUDP::setCallback(s8 (*callback)(u8 cmd, u8 *data, u8 size, u8 *res))
{
    mUDP.begin(7770);
    mCallback = callback;
}

void ControlUDP::sendResponse(bool ok, u8 cmd, u8 *data, u8 size)
{
    u8  buf[6 + size];
    u8  chksum = 0;

    mUDP.beginPacket(mUDP.remoteIP(), mUDP.remotePort());

    buf[0] = '$';
    buf[1] = 'M';
    buf[2] = (ok ? '>' : '!');
    buf[3] = size;
    buf[4] = cmd;
    memcpy(&buf[5], data, size);

    for (int i = 3; i < 5 + size; i++)
        chksum ^= buf[i];

    buf[5 + size] = chksum;

    mUDP.write(buf, 6 + size);
    mUDP.endPacket();
}

void ControlUDP::sendCmd(u8 cmd, u8 *data, u8 size)
{
#if 0
    putChar2TX('$');
    putChar2TX('M');
    putChar2TX('<');
    chkSumTX = 0;
    putChar2TX(size);
    putChar2TX(cmd);
    for (u8 i = 0; i < size; i++)
        putChar2TX(*data++);
    putChar2TX(chkSumTX);
#endif
}

void ControlUDP::evalCommand(u8 cmd, u8 *data, u8 size)
{
    u8  buf[30];

//    LOG("---- %d ----\n", cmd);
//    Utils::dump(data, size);

    if (mCallback) {
        memset(buf, 0, sizeof(buf));

        s8 ret = (*mCallback)(cmd, data, size, buf);
        if (ret >= 0) {
            sendResponse(TRUE, cmd, buf, ret);
        }
    }
}

void ControlUDP::handleRX(void)
{
    int rxSize = mUDP.parsePacket();

    if (rxSize == 0)
        return;

    while (rxSize--) {
        u8 ch;

        mUDP.read(&ch, 1);

        switch (mState) {
            case STATE_IDLE:
                if (ch == '$')
                    mState = STATE_HEADER_START;
                break;

            case STATE_HEADER_START:
                mState = (ch == 'M') ? STATE_HEADER_M : STATE_IDLE;
                break;

            case STATE_HEADER_M:
                mState = (ch == '<') ? STATE_HEADER_ARROW : STATE_IDLE;
                break;

            case STATE_HEADER_ARROW:
                if (ch > MAX_PACKET_SIZE) { // now we are expecting the payload size
                    mState = STATE_IDLE;
                    continue;
                }
                mDataSize = ch;
                mCheckSum = ch;
                mOffset   = 0;
                mState    = STATE_HEADER_SIZE;
                break;

            case STATE_HEADER_SIZE:
                mCmd       = ch;
                mCheckSum ^= ch;
                mState     = STATE_HEADER_CMD;
                break;

            case STATE_HEADER_CMD:
                if (mOffset < mDataSize) {
                    mCheckSum           ^= ch;
                    mRxPacket[mOffset++] = ch;
                } else {
                    if (mCheckSum == ch)
                        evalCommand(mCmd, mRxPacket, mDataSize);
                    mState = STATE_IDLE;
                    //rxSize = 0;             // no more than one command per cycle
                }
                break;
        }
    }
}
