#include "Servers.h"

void Servers::startWiFi(void)
{
    u8          ctrAP = 0;
    IPAddress   apIP(192, 168, 2, 1);


    // Start a Wi-Fi access point, and try to connect to some given access points.
    // Then wait for either an AP or STA connection

    WiFi.softAP(STR_SOFTAP_SSID, STR_SOFTAP_PASSWD);            // Start the access point
    WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
    LOG("Access Point : \"%s\" started\r\n", STR_SOFTAP_SSID);

    if (STR_AP1_SSID != NULL) {
        mWiFiMulti.addAP(STR_AP1_SSID, STR_AP1_PASSWD);
        ctrAP++;
    }
    if (STR_AP2_SSID != NULL) {
        mWiFiMulti.addAP(STR_AP2_SSID, STR_AP2_PASSWD);
        ctrAP++;
    }
    if (STR_AP3_SSID != NULL) {
        mWiFiMulti.addAP(STR_AP3_SSID, STR_AP3_PASSWD);
        ctrAP++;
    }

    LOG("Connecting\n");                               // Wait for the Wi-Fi to connect
    if (ctrAP > 0) {
        while (mWiFiMulti.run() != WL_CONNECTED && WiFi.softAPgetStationNum() < 1) {
            delay(250);
            LOG(".");
        }
    }
    LOG("\r\n");

    if (ctrAP > 0 && WiFi.softAPgetStationNum() == 0) {         // If the ESP is connected to an AP
        LOG("Connected to %s\nIP address:\t%s", WiFi.SSID().c_str(), WiFi.localIP().toString().c_str());
    } else {                                                    // If a station is connected to the ESP SoftAP
        LOG("Station connected to ESP8266 AP");
    }
    LOG("\r\n");
}

void Servers::startOTA(void)
{
    ArduinoOTA.setHostname(STR_OTA_NAME);
    ArduinoOTA.setPassword(STR_OTA_PASSWD);

    ArduinoOTA.onStart([]() {
        LOG("Start\n");
    });
    ArduinoOTA.onEnd([]() {
        LOG("\r\nEnd\n");
    });
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
        LOG("Progress: %u%%\r", (progress / (total / 100)));
    });
    ArduinoOTA.onError([](ota_error_t error) {
        LOG("Error[%u]: ", error);
        if (error == OTA_AUTH_ERROR)
            LOG("Auth Failed\n");
        else if (error == OTA_BEGIN_ERROR)
            LOG("Begin Failed\n");
        else if (error == OTA_CONNECT_ERROR)
            LOG("Connect Failed\n");
        else if (error == OTA_RECEIVE_ERROR)
            LOG("Receive Failed\n");
        else if (error == OTA_END_ERROR)
            LOG("End Failed\n");
    });
    ArduinoOTA.begin();
    LOG("OTA ready\r\n");
}

void Servers::startSPIFFS(void)
{
    SPIFFS.begin();
    LOG("SPIFFS started. Contents:\n");
    {
        Dir dir = SPIFFS.openDir("/");
        while (dir.next()) {
            String fileName = dir.fileName();
            size_t fileSize = dir.fileSize();
            LOG("\tFS File: %s, size: %s\r\n", fileName.c_str(), formatBytes(fileSize).c_str());
        }
        LOG("\n");
    }
}

void Servers::startMDNS(void)
{
    MDNS.begin(STR_mDNS_NAME);                                  // start the multicast domain name mWebServer
    LOG("mDNS responder started: http://");
    LOG(STR_mDNS_NAME);
    LOG(".local\n");
}

void Servers::setup(void)
{
    startWiFi();
    startOTA();
    startSPIFFS();
    startMDNS();

    // upload page
    mWebServer.on("/upload", HTTP_GET, [this](){
        if (!handleFileRead("/upload.html"))
            mWebServer.send(404, "text/plain", "FileNotFound");
    });
    mWebServer.on("/upload", HTTP_POST, [this]() {
        mWebServer.send(200, "text/plain", "");
        }, std::bind(&Servers::handleFileUpload, this));

    mWebServer.on("/list", HTTP_GET, std::bind(&Servers::handleFileList, this));

    // edit page
    mWebServer.on("/edit", HTTP_GET, [this](){
        if (!handleFileRead("/edit.html"))
            mWebServer.send(404, "text/plain", "FileNotFound");
    });
    mWebServer.on("/edit",  HTTP_POST, [this]() {
        mWebServer.send(200, "text/plain", "");
        }, std::bind(&Servers::handleFileList, this));
    mWebServer.on("/edit", HTTP_PUT, std::bind(&Servers::handleFileCreate, this));
    mWebServer.on("/edit", HTTP_DELETE, std::bind(&Servers::handleFileDelete, this));

    // etc
    mWebServer.onNotFound(std::bind(&Servers::handleNotFound, this));   // if someone requests any other file or page, go to function 'handleNotFound'
                                                                        // and check if the file exists
    mWebServer.begin();                                                 // start the HTTP WebServer
    LOG("HTTP WebServer started.\n");
}

void Servers::loop(void)
{
    mWebServer.handleClient();
    ArduinoOTA.handle();
}

/*__________________________________________________________SERVER_HANDLERS__________________________________________________________*/

void Servers::handleNotFound(void)
{
    // if the requested file or page doesn't exist, return a 404 not found error
    if (!handleFileRead(mWebServer.uri())){                     // check if the file exists in the flash memory (SPIFFS), if so, send it
        mWebServer.send(404, "text/plain", "404: File Not Found");
    }
}

void Servers::handleFileList(void)
{
    if (!mWebServer.hasArg("dir")) {
        mWebServer.send(500, "text/plain", "BAD ARGS");
        return;
    }

    String path = mWebServer.arg("dir");
    LOG("handleFileList: %s \n", path.c_str());
    Dir dir = SPIFFS.openDir(path);
    path = String();

    String output = "[";
    while (dir.next()){
        File entry = dir.openFile("r");
        if (output != "[") output += ',';
        bool isDir = false;
        output += "{\"type\":\"";
        output += (isDir)?"dir":"file";
        output += "\",\"name\":\"";
        output += String(entry.name()).substring(1);
        output += "\"}";
        entry.close();
    }

    output += "]";
    mWebServer.send(200, "text/json", output);
}

bool Servers::handleFileRead(String path)
{
    // send the right file to the client (if it exists)
    LOG("handleFileRead: %s\n", path.c_str());
    if (path.endsWith("/"))
        path += "index.html";                                   // If a folder is requested, send the index file

    String contentType = getContentType(path);                  // Get the MIME type
    String pathWithGz = path + ".gz";
    if (SPIFFS.exists(pathWithGz) || SPIFFS.exists(path)) {     // If the file exists, either as a compressed archive, or normal
        if (SPIFFS.exists(pathWithGz))                          // If there's a compressed version available
            path += ".gz";                                      // Use the compressed verion
        File file = SPIFFS.open(path, "r");                     // Open the file
        size_t sent = mWebServer.streamFile(file, contentType); // Send it to the client
        file.close();                                           // Close the file again
        LOG("\tSent file: %s\n", path.c_str());
        return true;
    }
    LOG("\tFile Not Found: %s\n", path.c_str());                // If the file doesn't exist, return false
    return false;
}

void Servers::handleFileUpload(void)
{
    // upload a new file to the SPIFFS
    HTTPUpload& upload = mWebServer.upload();
    String path;
    if (upload.status == UPLOAD_FILE_START){
        path = upload.filename;
        if (!path.startsWith("/"))
            path = "/" + path;
        if (!path.endsWith(".gz")) {                            // The file mWebServer always prefers a compressed version of a file
            String pathWithGz = path+".gz";                     // So if an uploaded file is not compressed, the existing compressed
            if (SPIFFS.exists(pathWithGz))                      // version of that file must be deleted (if it exists)
                SPIFFS.remove(pathWithGz);
        }
        LOG("handleFileUpload Name: %s\n", path.c_str());
        mFileUpload = SPIFFS.open(path, "w");                   // Open the file for writing in SPIFFS (create if it doesn't exist)
        path = String();
    } else if (upload.status == UPLOAD_FILE_WRITE) {
        if (mFileUpload)
            mFileUpload.write(upload.buf, upload.currentSize);  // Write the received bytes to the file
    } else if (upload.status == UPLOAD_FILE_END) {
        if (mFileUpload) {                                      // If the file was successfully created
            mFileUpload.close();                                // Close the file again
            LOG("handleFileUpload Size: %ld\n", upload.totalSize);
            mWebServer.sendHeader("Location","/success.html");  // Redirect the client to the success page
            mWebServer.send(303);
        } else {
            mWebServer.send(500, "text/plain", "500: couldn't create file");
        }
    }
}

void Servers::handleFileDelete(void)
{
    if (mWebServer.args() == 0)
        return mWebServer.send(500, "text/plain", "BAD ARGS");

    String path = mWebServer.arg(0);
    LOG("handleFileDelete: %s\n", path.c_str());
    if (path == "/")
        return mWebServer.send(500, "text/plain", "BAD PATH");
    if (!SPIFFS.exists(path))
        return mWebServer.send(404, "text/plain", "FileNotFound");
    SPIFFS.remove(path);
    mWebServer.send(200, "text/plain", "");
    path = String();
}

void Servers::handleFileCreate(void)
{
    if(mWebServer.args() == 0)
        return mWebServer.send(500, "text/plain", "BAD ARGS");

    String path = mWebServer.arg(0);
    LOG("handleFileCreate: %s\n", path.c_str());
    if (path == "/")
        return mWebServer.send(500, "text/plain", "BAD PATH");
    if (SPIFFS.exists(path))
        return mWebServer.send(500, "text/plain", "FILE EXISTS");
    File file = SPIFFS.open(path, "w");
    if (file)
        file.close();
    else
        return mWebServer.send(500, "text/plain", "CREATE FAILED");
    mWebServer.send(200, "text/plain", "");
    path = String();
}

/*__________________________________________________________HELPER_FUNCTIONS__________________________________________________________*/
String Servers::formatBytes(size_t bytes)
{
    if (bytes < 1024) {
        return String(bytes) + "B";
    } else if (bytes < (1024 * 1024)) {
        return String(bytes / 1024.0) + "KB";
    } else if (bytes < (1024 * 1024 * 1024)) {
        return String(bytes / 1024.0 / 1024.0) + "MB";
    }
}

String Servers::getContentType(String filename)
{
    if (filename.endsWith(".html"))
        return "text/html";
    else if (filename.endsWith(".css"))
        return "text/css";
    else if (filename.endsWith(".js"))
        return "application/javascript";
    else if (filename.endsWith(".ico"))
        return "image/x-icon";
    else if (filename.endsWith(".gz"))
        return "application/x-gzip";
    return "text/plain";
}