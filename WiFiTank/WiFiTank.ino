#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ArduinoOTA.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <FS.h>
#include <WebSocketsServer.h>
#include "common.h"
#include "Servers.h"
#include "ControlUDP.h"
#include "ByteBuffer.h"
#include "utils.h"

#define PIN_MOT_RIGHT_PWM   D1
#define PIN_MOT_RIGHT_IN2   D2
#define PIN_MOT_RIGHT_IN1   D4
#define PIN_MOT_LEFT_IN1    D6
#define PIN_MOT_LEFT_IN2    D7
#define PIN_MOT_LEFT_PWM    D8
#define PIN_BATT_ADC        A0

#define BATT_BASE_VOLT      34      // 3.4v 
#define BATT_BASE_ADC       740
#define BATT_INTERVAL       22      // 0.1v interval

static const u8 tblPins[] = {
    PIN_MOT_RIGHT_IN1,
    PIN_MOT_RIGHT_IN2,
    PIN_MOT_RIGHT_PWM,
    PIN_MOT_LEFT_PWM,
    PIN_MOT_LEFT_IN2,
    PIN_MOT_LEFT_IN1,
};

static Servers          mServers;
static WebSocketsServer mWebSocket = WebSocketsServer(81);
static ControlUDP       mCtrl;
static u8               mClientNum = 0xff;
static u32              mLastTick = 0;
static u32              mCtr = 0;
static u16              mOldSpdL = 1500;
static u16              mOldSpdR = 1500;
static u32              mLastUpdated = 0;
static u16              mAuxSw[4];

static void writeMotor(u8 pin_in1, u8 pin_in2, u8 pin_pwm, u16 spd, u16 *old)
{
    u8  in1, in2;
    u16 pwm;
    
    if ((spd > 1500 && *old < 1500) || (spd < 1500 && *old > 1500)) {
        digitalWrite(pin_in2, LOW);
        digitalWrite(pin_in1, LOW);
        analogWrite(pin_pwm, 1023);
        delay(1);
    }

    if (spd > 1500) {
        in1 = HIGH;
        in2 = LOW;
        pwm = map(spd - 1500, 0, 500, 0, 1023);
    } else if (spd < 1500) {
        in1 = LOW;
        in2 = HIGH;
        pwm = map(1500 - spd, 0, 500, 0, 1023);
    } else {
        in1 = LOW;
        in2 = LOW;
        pwm = 1023;
    }    
    
    if (spd != *old) {
        digitalWrite(pin_in1, in1);
        digitalWrite(pin_in2, in2);
        analogWrite(pin_pwm, pwm);
        *old = spd;
    }

#if 0
    if (pin_in1 == PIN_MOT_LEFT_IN2) {
        LOG("LEFT  : ");
    } else {
        LOG("RIGHT : ");
    }
    LOG("speed:%4d, in1:%2d, in2:%2d, pwm:%4d\n", spd, in1, in2, pwm);
#endif
}

static void setMotors(u16 spdL, u16 spdR)
{
    writeMotor(PIN_MOT_LEFT_IN2,  PIN_MOT_LEFT_IN1,  PIN_MOT_LEFT_PWM,  spdL, &mOldSpdL);  // left motor is reversed
    writeMotor(PIN_MOT_RIGHT_IN1, PIN_MOT_RIGHT_IN2, PIN_MOT_RIGHT_PWM, spdR, &mOldSpdR);
}

static void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght)
{
    switch (type) {
        case WStype_DISCONNECTED:                               // if the websocket is disconnected
            mClientNum = 0xff;
            LOG("[%u] Disconnected!\n", num);
            break;

        case WStype_CONNECTED: {                                // if a new websocket connection is established
                IPAddress ip = mWebSocket.remoteIP(num);
                LOG("[%u] Connected from %s url: %s\n", num, ip.toString().c_str(), payload);
                mClientNum = num;
            }
            break;

        case WStype_TEXT:                                       // if new text data is received
            if (payload[0] == '#') {
                u32 speed = (u32) strtol((const char *) &payload[1], NULL, 16);
                u16 l = ((speed >> 16) & 0xFFFF);
                u16 r =          speed & 0xFFFF;
                setMotors(l, r);
            }
            break;
    }
}


#define VBAT_SMOOTH_LEVEL       16
static u16  mVoltBuf[VBAT_SMOOTH_LEVEL];
static u16  mVoltSum;
static u8   mVoltIdx;
static u8   mVoltBatt;

static u8 getBattVolt(void)
{
    u16 v;

    v = analogRead(PIN_BATT_ADC);

    mVoltSum += v;
    mVoltSum -= mVoltBuf[mVoltIdx];
    mVoltBuf[mVoltIdx++] = v;
    mVoltIdx %= VBAT_SMOOTH_LEVEL;

    v = mVoltSum / VBAT_SMOOTH_LEVEL;
    if (v >= BATT_BASE_ADC) {
        v = BATT_BASE_VOLT + (v - BATT_BASE_ADC) / BATT_INTERVAL;
    } else {
        v = 0;
    }
    
    return v;
}

static s8 handleMSP(u8 cmd, u8 *data, u8 size, u8 *res)
{
    u16 thr;
    u16 roll;
    u16 pitch;
    u16 yaw;
    u8  flag = 0;
    s8  ret  = -1;

    ByteBuffer inData(data, size);
    ByteBuffer outData(res, 20);

    if (!res)
        return ret;

    switch (cmd) {
        case ControlUDP::MSP_IDENT:
            outData.put8(240);
            outData.put8(0);
            outData.put8(0);
            outData.put32(0);
            ret = outData.pos();
            break;
        
        case ControlUDP::MSP_STATUS:
            outData.put16(0);
            outData.put16(0);
            outData.put16(0);
            outData.put32(1);
            outData.put8(0);
            ret = outData.pos();
            break;
            
        case ControlUDP::MSP_ANALOG:
            outData.put8(mVoltBatt);
            outData.put16(0);
            outData.put16(0);
            outData.put16(0);
            ret = outData.pos();
            break;
            
        case ControlUDP::MSP_MISC:
            outData.put16(0);       // initPowerTrigger
            outData.put16(1020);    // min throttle
            outData.put16(2000);    // max throttle
            outData.put16(1000);    // min command
            outData.put16(1000);    // failsafe_throttle
            outData.put16(0);       // arm counter
            outData.put32(0);       // lifetime
            outData.put16(0);       // mag_declination
            outData.put8(1);        // vbatscale
            outData.put8(36);       // vbat_warn1
            outData.put8(35);       // vbat_warn2
            outData.put8(34);       // vbat_crit
            ret = outData.pos();
            break;

        case ControlUDP::MSP_SET_RAW_RC: {
                if (size == 0)
                    break;
                
                roll   = inData.get16();
                pitch  = inData.get16();
                yaw    = inData.get16();
                thr    = inData.get16();
                mAuxSw[0] = inData.get16();
                mAuxSw[1] = inData.get16();
                mAuxSw[2] = inData.get16();
                mAuxSw[3] = inData.get16();
                //LOG("thr:%5d, yaw:%5d, pitch:%5d, roll:%5d, aux1:%5d\n", thr, yaw, pitch, roll, aux1);
                
                //if (millis() - mLastUpdated > 40) {
                    setMotors(thr, pitch);
                //    mLastUpdated = millis();
                //}
            }
            break;
    }
    return ret;
}

void setup() {
    for (u8 i = 0; i < sizeof(tblPins); i++) {
        pinMode(tblPins[i], OUTPUT);
    }
    setMotors(mOldSpdL, mOldSpdR);

    Serial.begin(115200);
    delay(10);
    LOG("\r\n");

    mServers.setup();
    mWebSocket.begin();
    mWebSocket.onEvent(webSocketEvent);
    LOG("WebSocket mWebServer started.");
    mCtrl.setCallback(handleMSP);
    
    mVoltIdx = 0;
    mVoltSum = 0;
    for (u8 i = 0; i < VBAT_SMOOTH_LEVEL; i++) {
        mVoltBatt = getBattVolt();
    }
}

void loop() {
    mWebSocket.loop();
    mServers.loop();
    mCtrl.handleRX();

    if (millis() - mLastTick > 1000) {
        mVoltBatt = getBattVolt();
        
        if (mClientNum != 0xff) {
            char buf[10];
            
            memset(buf, 0, sizeof(buf));
            if (mVoltBatt >= BATT_BASE_VOLT) {
                sprintf(buf, "%d.%d V", mVoltBatt / 10, mVoltBatt % 10);
            } else {
                sprintf(buf, "bad");
            }
            mWebSocket.sendTXT(mClientNum, buf);
        }
        mLastTick = millis();
    }
}
